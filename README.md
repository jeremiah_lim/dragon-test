# test project 
Project using swagger-node, which can be clone from https://github.com/swagger-api/swagger-node.

controller can be found inside /api/controllers/vd.js

model can be found inside /api/models/vault.js

routing and definition rules can be found inside /api/swagger/swagger.yaml

live demo url can be found at http://jeremiah-dragon-test.herokuapp.com/object