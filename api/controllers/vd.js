
'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
  require underscoreJS library 
*/

var _= require('underscore');
var db= require('../../db.js'); 

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

  In the  project the 'get' operation on the '/' path has an operationId named 'get'.  Here,
  we specify that in the exports of this module that 'get' maps to the function named 'getKey'
  and 'post' maps to the function named 'postKey'
 */
module.exports = {
  get: getKey,
  post: postKey
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */

// method to get details from a given key
function getKey(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  var key = req.swagger.params.key.value || '';
  var timestamp = req.swagger.params.timestamp.value || '';

  // if key is empty
  if (_.isEmpty(key)) {
      return res.status(400).json({message:"key cannot be empty"});
  }

  var search = {key:key};

  // if timestamp was given, let's add it to the search query
  if (!_.isEmpty(timestamp)) {
      search.timestamp= timestamp;
  }

  // Now let's find the item, limit to 1 and order by timestamp desc so we will always get the latest value
  db.vault.findOne({where:search, order:"timestamp DESC"}).then(function(record) {
    if (!record) {
      return res.status(404).json({message:"no records found for this key"});
    }else{
      return res.json(record.toPublicJSON() );
    }
  },function(error) {
    return res.status(500).send(error);
  });

}

// method to post key/value OR update a key's value if key name already exists in DB
function postKey(req, res) {
  /*
    Although variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    since we are accessing a string map where the key name can be anything, we will do things a little 
    different and access all the values passed through using req.swagger.params.param.value property
  */

  var params = req.swagger.params.param.value||{};

  if(_.isEmpty(params) ) {
    // set error message and exit
    return res.status(400).json({message:"No value was given"});
  }

  // now we convert params to list of key value pairs and take the first one
  // since the test only specifies one key/value pair
  var pair = _.first( _.pairs(params) );

  // we got the key 
  var pairKey = pair[0];
  // and the value
  var pairValue = pair[1]; 

  // make sure that key and value is not empty
  if (_.isEmpty(pairKey) || _.isEmpty(pairValue) ) {
    return res.status(400).json({message:"key/value given cannot be empty"});
  }
  
  // set up the record to insert
  var item = {key:pairKey, value:pairValue};
  // now let's insert the key value into the DB
  db.vault.create(item).then(function(record) {
    // return formatted json reply
    return res.json(record.toPublicJSON() );
  },function (error) {
    // server error
    return res.status(500).send(error);
  });
}
