var _ = require('underscore');
module.exports= function (sequelize, Datatypes) {
	var vault = sequelize.define('vault',{
		key:{
			type: Datatypes.STRING,
			allowNull:false,
			validate:{
				len:[1,255]
			},
			// setter function to update timestamp to current time on new record creation
			set:function(input) {
				this.setDataValue('key', input);
				// set timestamp entry to now in UNIX format
				this.setDataValue('timestamp', (new Date().getTime() / 1000).toFixed(0) );
			}
		},
		value:{
			type: Datatypes.STRING,
			allowNull:false,
			validate:{
				len:[1,255]
			}
		},
		timestamp:{
			type:Datatypes.INTEGER,
			allowNull:false
		}
	},{
		// add hook to make sure all keys are standardize to lower case
		hooks: {
            beforeValidate: function (vault, options) {
                if (typeof vault.key === 'string') {
                    vault.key = vault.key.toLowerCase();
                }
            }
        },
        // just good practice to add a toPublicJson method, allows to format future
        // json strings easily in one place
        instanceMethods: {
            toPublicJSON: function () {
                var json = this.toJSON();
                // change timestamp to string format
                json.timestamp = json.timestamp.toString();
                var userKey = json.key;
                var userValue = json.value;
                json[userKey]=userValue;
                return _.pick(json, userKey , 'timestamp' );
            }
        }
	});

	return vault;
};