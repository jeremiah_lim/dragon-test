'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
// for testing
module.exports = app; 
// require db to force sync
var db= require('./db.js'); 

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;

	db.sequelize.sync({force:true}).then(function () {

		app.listen(port, function () {
		    console.log('Express listening on port: ' + port);
		});	
	});

});
